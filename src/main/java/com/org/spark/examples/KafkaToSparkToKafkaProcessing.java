/*
 * This class is used to take sensor data from kafka ,tranform it i.e. convert csv to json and write it again on kafka.
 */
package com.org.spark.examples;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.org.spark.model.Sensor;

import kafka.serializer.StringDecoder;
import scala.Tuple2;

public class KafkaToSparkToKafkaProcessing {

	public static void main(String args[]) throws InterruptedException {
		if (args.length < 12) {
			System.exit(0);
		}
		String topics = args[0];
		System.setProperty("hadoop.home.dir", "D:\\Hadoop");
		String bootstrapServers = args[1];
		Set<String> topicsSet = new HashSet<>(Arrays.asList(topics.split(",")));
		SparkConf sc = new SparkConf().setMaster(args[2]).setAppName(args[3]);
		JavaStreamingContext jssc = new JavaStreamingContext(sc, Durations.seconds(2));
		Map<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("bootstrap.servers", bootstrapServers);
		kafkaParams.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		kafkaParams.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		kafkaParams.put("group.id", args[4]);
		kafkaParams.put("auto.offset.reset", args[5]);
		kafkaParams.put("enable.auto.commit", args[6]);
		Properties props = new Properties();
		props.put("bootstrap.servers", bootstrapServers);
		props.put("acks", args[7]);
		props.put("retries", args[8]);
		props.put("batch.size", args[9]);
		props.put("linger.ms", args[10]);
		props.put("buffer.memory", args[11]);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		JavaPairInputDStream<String, String> stream = KafkaUtils.createDirectStream(jssc, String.class, String.class,
				StringDecoder.class, StringDecoder.class, kafkaParams, topicsSet);
		JavaPairDStream<String, Sensor> javaPairDStream = stream
				.mapToPair(new PairFunction<Tuple2<String, String>, String, Sensor>() {
					@Override
					public Tuple2<String, Sensor> call(Tuple2<String, String> record) throws Exception {
						System.out.println(
								"===================" + Long.toString(System.currentTimeMillis()) + "," + record._2);
						String coloumns[] = record._2.split(",");
						Sensor sensor = Sensor.builder().unitNumber(coloumns[0]).cycles(coloumns[1])
								.operationalSettingsOne(coloumns[2]).operationalSettingsTwo(coloumns[3])
								.operationalSettingsThree(coloumns[4]).sensor1(coloumns[5]).sensor2(coloumns[6])
								.sensor3(coloumns[7]).sensor4(coloumns[8]).sensor5(coloumns[9]).sensor6(coloumns[10])
								.sensor7(coloumns[11]).sensor8(coloumns[12]).sensor9(coloumns[13])
								.sensor10(coloumns[14]).sensor11(coloumns[15]).sensor12(coloumns[16])
								.sensor13(coloumns[17]).sensor14(coloumns[18]).sensor15(coloumns[19])
								.sensor16(coloumns[20]).sensor17(coloumns[21]).sensor18(coloumns[22])
								.sensor19(coloumns[23]).sensor20(coloumns[24]).build();
						return new Tuple2<>(Long.toString(System.currentTimeMillis()), sensor);
					}
				});
		javaPairDStream.foreachRDD(jPairRDD -> {
			jPairRDD.foreach(rdd -> {
				System.out.println("===============++++++++++" + rdd._1 + "----------------> " + rdd._2);
				Producer<String, String> producer = new KafkaProducer<>(props);
				Gson gson = new GsonBuilder().create();
				producer.send(new ProducerRecord<String, String>(args[12], Long.toString(System.currentTimeMillis()),
						gson.toJson(rdd._2)));
				producer.close();
			});
		});
		jssc.start();
		jssc.awaitTermination();
	}
}
