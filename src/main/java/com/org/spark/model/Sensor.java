package com.org.spark.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class Sensor {
	private String unitNumber;
	private String cycles;
	private String operationalSettingsOne;
	private String operationalSettingsTwo;
	private String operationalSettingsThree;
	private String sensor1;
	private String sensor2;
	private String sensor3;
	private String sensor4;
	private String sensor5;
	private String sensor6;
	private String sensor7;
	private String sensor8;
	private String sensor9;
	private String sensor10;
	private String sensor11;
	private String sensor12;
	private String sensor13;
	private String sensor14;
	private String sensor15;
	private String sensor16;
	private String sensor17;
	private String sensor18;
	private String sensor19;
	private String sensor20;
	private String sensor21;
	private String sensor22;
	private String sensor23;
	private String sensor24;
}
